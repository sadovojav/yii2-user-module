# Yii2 user

[Yii2](http://www.yiiframework.com) user modules

#### Features:
- Console RBAC
- Default roles [user, manager, admin]
- Social authorisation/registration
- User managment

### Composer

The preferred way to install this extension is through [Composer](http://getcomposer.org/).

Either run ```php composer.phar require sadovojav/yii2-user-module ""dev-master"```

or add ```"sadovojav/yii2-user-module": ""dev-master"``` to the require section of your ```composer.json```

### Migration

yii migrate --migrationPath=@vendor/sadovojav/yii2-user-module/migrations

### Using

1. Attach the module in your config file:

```php
'modules' => [
    'user' => [
        'class' => 'sadovojav\user\Module',
        'access' => ['backend']
    ],
],
'components' => [
    'user' => [
        'class' => 'yii\web\User',
        'identityClass' => 'sadovojav\user\models\User',
        'loginUrl' => ['login']
    ],
],
```

2. Add url rules to frontend:
```php
'<_a:(login|logout)>' => 'user/auth/<_a>',
'<_a:(register|confirm)>' => 'user/registration/<_a>',
'user/<_a:(view|update)>' => 'user/user/<_a>',
'password/<_a:(request-reset|reset)>' => 'user/password/<_a>',
```

3. Add url rules to backend:
```php
'<action:(login|logout|captcha)>' => 'user/admin/auth/<action>',
'user/<action:(view|update|delete)>/<id:\d+>' => 'user/admin/user/<action>',
'users' => 'user/admin/user/index',
```

#### Module parameters

- `access` = `@` - access roles to administration
- `modelMap` = [] - models map
- `commandControllerNamespace` = `sadovojav\user\commands` - command controller namespace
- `mailViewPath` = `@sadovojav/user/views/mail` - mail view path
- `enableRegistration` = `false` - enable/disable registration on frontend
- `enableSocialAuth` = `false` - enable/disable social auth
- `passwordResetTokenExpire` = `43200` - password reset token expire
- `rememberFor` = `1209600` - time to remember login user
- `captchaAttempts` = `3` - captcha attempts


#### Module models

```php
'User' => 'sadovojav\user\model\User',
'UserSearch' => 'sadovojav\user\model\UserSearch',
'UserProfile' => 'sadovojav\user\model\UserProfile',
'LoginForm' => 'sadovojav\user\model\LoginForm',
'Query' => 'sadovojav\user\model\Query',
```

Default user: `admin/admin`