<?php

use sadovojav\user\Module;

$this->title = $user->username;
$this->params['breadcrumbs'] = [
    ['label' => $user->username, 'url' => ['view', 'id' => $user->id]],
    Module::t('user', 'Update'),
];

?>

<div class="user-update" style="margin-top: 5%">
    <?= $this->render('_form', [
        'user' => $user,
        'profile' => $profile
    ]); ?>
</div>
