<?php

use yii\helpers\Html;
use sadovojav\user\Module;
use yii\widgets\ActiveForm;

?>

<div class="user-form">
    <div class="panel panel-primary center-block" style="width: 700px">
        <div class="panel-heading">
            <h3 class="panel-title"><?= Module::t('user', 'Update'); ?></h3>
        </div>

        <?php $form = ActiveForm::begin([
            'id' => 'user-form',
            'enableClientValidation' => false
        ]); ?>

        <div class="panel-body">
            <?= $form->field($profile, 'name_first')->textInput([
                'maxlength' => 50
            ]); ?>

            <?= $form->field($profile, 'name_last')->textInput([
                'maxlength' => 50
            ]); ?>

            <?= $form->field($user, 'password')->passwordInput([
                'maxlength' => 255,
                'value' => ''
            ]); ?>

            <?= $form->field($user, 'passwordRepeat')->passwordInput([
                'maxlength' => 255,
                'value' => ''
            ]); ?>

        </div>

        <div class="panel-footer text-right">
            <?= Html::submitButton(Module::t('user', 'Save'), [
                'class' => 'btn btn-primary'
            ]); ?>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
</div>
