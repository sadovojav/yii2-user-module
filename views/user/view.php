<?php

use yii\helpers\Html;
use sadovojav\user\Module;
use yii\widgets\DetailView;

$this->title = Module::t('user', 'View');
$this->params['breadcrumbs'] = [
    $this->title,
];

?>

<div class="user-view" style="margin-top: 5%">
    <div class="panel panel-primary center-block" style="width: 700px">
        <div class="panel-heading">
            <h3 class="panel-title"><?= $model->username; ?></h3>
        </div>

        <div class="panel-body">
            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'id',
                    'username',
                    'profile.name_first',
                    'profile.name_last',
                    'email:email',
//                    'auth_key',
//                    'password',
                    [
                        'attribute' => 'role',
                        'value' => $model->roleName
                    ],
                    'status:boolean',
                    'created_at',
                    'updated_at',
                ],
            ]); ?>
        </div>

        <div class="panel-footer text-right">
            <?= Html::a(Module::t('user', 'Update'), ['update'], ['class' => 'btn btn-primary']); ?>
        </div>
    </div>
</div>
