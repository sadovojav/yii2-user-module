<?php

use yii\helpers\Html;
use sadovojav\user\Module;
use yii\widgets\ActiveForm;

?>

<div class="user user-registration-register">
    <?php $form = ActiveForm::begin([
        'action' => ['/user/registration/register'],
        'id' => 'reset-password-form',
        'enableClientValidation' => false
    ]); ?>

    <?= $form->field($user, 'username')->textInput([
        'maxlength' => 32,
    ]); ?>

    <?= $form->field($user, 'email')->textInput([
        'maxlength' => 32
    ]); ?>

    <?= $form->field($user, 'password')->passwordInput([
        'maxlength' => 255,
        'value' => ''
    ]); ?>

    <?= $form->field($user, 'passwordRepeat')->passwordInput([
        'maxlength' => 255,
        'value' => ''
    ]); ?>

    <?= Html::submitButton(Module::t('user', 'Register'), [
        'class' => 'register-btn'
    ]); ?>

    <?php ActiveForm::end(); ?>

    <br/>

    <?= Html::a(Module::t('user', 'Login'), ['/user/auth/login']); ?>
</div>