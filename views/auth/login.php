<?php

use yii\helpers\Html;
use sadovojav\user\Module;
use yii\bootstrap\ActiveForm;
use sadovojav\user\widgets\AuthChoice;

$this->title = Module::t('user', 'Log in');

?>

<div class="user user-auth-login">
    <section class="login-section">
        <?php $form = ActiveForm::begin([
            'action' => ['/user/auth/login'],
            'id' => 'login-form',
            'enableClientValidation' => false
        ]); ?>

        <?= $form->field($model, 'username')->textInput([
            'placeholder' => $model->getAttributeLabel('username')
        ])->label(false); ?>

        <?= $form->field($model, 'password')->passwordInput([
            'placeholder' => $model->getAttributeLabel('password')
        ])->label(false); ?>

        <?= $form->field($model, 'rememberMe')->checkbox() ?>

        <?= Html::submitButton(Module::t('user', 'Login'), [
            'class' => 'login-btn'
        ]); ?>

        <?php ActiveForm::end(); ?>

        <br/>

        <?= Html::a(Module::t('user', 'Reset password'), ['/user/password/request-reset']); ?>

        <?php if (Module::getInstance()->enableRegistration) : ?>
            <section class="">
                <?= Html::a(Module::t('user', 'Do not have account? Sign up!'), ['/user/registration/register']); ?>
            </section>
        <?php endif; ?>

        <?php if (Module::getInstance()->enableSocialAuth) : ?>
            <?= AuthChoice::widget([
                'baseAuthUrl' => ['/user/auth/social'],
            ]); ?>
        <?php endif; ?>
    </section>
</div>
