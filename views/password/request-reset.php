<?php

use yii\helpers\Html;
use sadovojav\user\Module;
use yii\widgets\ActiveForm;

$this->title = Module::t('user', 'Password reset request');

?>

<div class="user-password-request-reset">
    <div class="panel panel-primary center-block" style="width: 500px">
        <div class="panel-heading">
            <h3 class="panel-title"><?= Module::t('user', 'Password reset request'); ?></h3>
        </div>

        <div class="panel-body">
            <p><?= Module::t('user', 'Please fill out your email. A link to reset password will be sent there'); ?>.</p>

            <?php $form = ActiveForm::begin([
                'id' => 'request-reset-password-form',
                'enableClientValidation' => false
            ]); ?>

            <?= $form->field($model, 'email')->textInput([
                'maxlength' => 32,
                'autofocus' => true
            ]); ?>

            <?= Html::submitButton(Module::t('user', 'Send'), [
                'class' => 'btn btn-primary btn-block'
            ]); ?>

            <?php ActiveForm::end(); ?>
        </div>

        <div class="panel-footer">
            <div class="text-center">
                <?= Html::a(Module::t('user', 'Login'), ['/user/auth/login']); ?>

                <br/>

                <?= Html::a(Module::t('user', 'Registration'), ['/user/registration/register']); ?>
            </div>
        </div>
    </div>
</div>