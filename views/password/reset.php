<?php

use yii\helpers\Html;
use sadovojav\user\Module;
use yii\widgets\ActiveForm;

$this->title = Module::t('user', 'Password reset');

?>

<div class="user-password-reset">
    <div class="panel panel-primary center-block" style="width: 500px">
        <div class="panel-heading">
            <h3 class="panel-title"><?= Module::t('user', 'Password reset'); ?></h3>
        </div>

        <div class="panel-body">
            <p><?= Module::t('user', 'Please choose your new password:'); ?></p>

            <?php $form = ActiveForm::begin([
                'id' => 'reset-password-form',
                'enableClientValidation' => false
            ]); ?>

            <?= $form->field($model, 'password')->passwordInput([
                'maxlength' => 32,
                'autofocus' => true
            ]); ?>

            <?= Html::submitButton(Module::t('user', 'Reset'), [
                'class' => 'btn btn-primary btn-block'
            ]); ?>

            <?php ActiveForm::end(); ?>
        </div>

        <div class="panel-footer">
            <div class="text-center">
                <?= Html::a(Module::t('user', 'Login'), ['/user/auth/login']); ?>

                <br/>

                <?= Html::a(Module::t('user', 'Registration'), ['/user/registration/register']); ?>
            </div>
        </div>
    </div>
</div>