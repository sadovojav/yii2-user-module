<?php

use sadovojav\user\Module;

?>

<?= Module::t('user', 'Hello'); ?>,

<?= Module::t('user', 'Your account on {0} has been created.', Yii::$app->name); ?>

<?= Yii::t('user', 'If you did not make this request you can ignore this email'); ?>.
