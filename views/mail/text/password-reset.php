<?php

use sadovojav\user\Module;

?>

<?= Module::t('user', 'Hello') ?>,

<?= Module::t('user', 'We have received a request to reset the password for your account on {0}', Yii::$app->name) ?>.

<?= Module::t('user', 'Please click the link below to complete your password reset') ?>.

<?= Yii::$app->urlManager->createAbsoluteUrl(['/user/password/reset', 'token' => $user->password_reset_token]); ?>

<?= Module::t('user', 'If you cannot click the link, please try pasting the text into your browser') ?>.

<?= Module::t('user', 'If you did not make this request you can ignore this email') ?>.
