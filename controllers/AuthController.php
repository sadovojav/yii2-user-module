<?php

namespace sadovojav\user\controllers;

use Yii;
use yii\authclient\AuthAction;
use yii\filters\AccessControl;
use sadovojav\user\models\User;
use sadovojav\user\models\UserAuth;
use sadovojav\user\models\forms\LoginForm;
use sadovojav\user\clients\ClientInterface;

/**
 * Class AuthController
 * @package sadovojav\user\controllers
 */
class AuthController extends \yii\web\Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['login', 'logout'],
                'rules' => [
                    [
                        'actions' => ['login'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@']
                    ],
                ],
                'denyCallback' => function () {
                    return $this->goHome();
                }
            ],
        ];
    }

    public function actions()
    {
        return [
            'social' => [
                'class' => AuthAction::className(),
                'successCallback' => Yii::$app->user->isGuest
                    ? [$this, 'authenticate']
                    : [$this, 'connect'],
            ],
        ];
    }

    public function actionLogin()
    {
        $model = new LoginForm();

        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    public function authenticate($client)
    {
        $attributes = $client->getUserAttributes();

        $auth = UserAuth::find()->where([
            'source' => $client->getId(),
            'source_id' => $attributes['id'],
        ])->one();

        if ($auth) {
            $user = $auth->user;

            Yii::$app->user->login($user);
        } else {
            if (isset($attributes['email']) && User::find()->where(['email' => $attributes['email']])->exists()) {
                Yii::$app->getSession()->setFlash('error', [
                    Yii::t('app', "User with the same email as in {client} account already exists but isn't linked to it. Login using email first to link it.", ['client' => $client->getTitle()]),
                ]);
            } else {
                $user = new User([
                    'username' => $attributes['login'],
                    'email' => $attributes['email'],
                    'password' => Yii::$app->security->generateRandomString(6)
                ]);

                $user->generateAuthKey();
                $user->generatePasswordResetToken();
                $transaction = $user->getDb()->beginTransaction();
                if ($user->save()) {
                    $auth = new UserAuth([
                        'user_id' => $user->id,
                        'source' => $client->getId(),
                        'source_id' => (string)$attributes['id'],
                    ]);

                    if ($auth->save()) {
                        $transaction->commit();

                        Yii::$app->user->login($user);
                    } else {
                        print_r($auth->getErrors());
                    }
                } else {
                    print_r($user->getErrors());
                }
            }
        }
    }

    public function connect(ClientInterface $client)
    {
        $attributes = $client->getUserAttributes();

        $auth = UserAuth::find()->where([
            'source' => $client->getId(),
            'source_id' => $attributes['id'],
        ])->one();

        if (!$auth) {
            $auth = new UserAuth([
                'user_id' => Yii::$app->user->id,
                'source' => $client->getId(),
                'source_id' => $attributes['id'],
            ]);

            $auth->save();
        }
    }
}
