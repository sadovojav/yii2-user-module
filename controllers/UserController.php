<?php

namespace sadovojav\user\controllers;

use Yii;
use yii\base\Model;
use yii\filters\AccessControl;
use sadovojav\user\models\User;
use yii\web\NotFoundHttpException;
use yii\web\ForbiddenHttpException;

/**
 * Class UserController
 * @package sadovojav\user\controllers
 */
class UserController extends \yii\web\Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['view', 'update'],
                'rules' => [
                    [
                        'actions' => ['view', 'update'],
                        'allow' => true,
                        'roles' => ['@']
                    ],
                ],
            ],
        ];
    }

    /**
     * Displays a single User model.
     * @return mixed
     */
    public function actionView()
    {
        return $this->render('view', [
            'model' => $this->findModel(Yii::$app->user->id),
        ]);
    }

    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionUpdate()
    {
        $user = $this->findModel(Yii::$app->user->id);
        $user->setScenario('update');
        $user->profile->setScenario('update');

        if ($user->load(Yii::$app->request->post()) && $user->profile->load(Yii::$app->request->post())) {
            if (Model::validateMultiple([$user, $user->profile])) {
                $user->save(false);

                $user->link('profile', $user->profile);

                return $this->redirect(['view', 'id' => $user->id]);
            }
        }

        return $this->render('update', [
            'user' => $user,
            'profile' => $user->profile
        ]);
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
