<?php

namespace sadovojav\user\controllers;

use Yii;
use yii\base\Model;
use yii\filters\AccessControl;
use sadovojav\user\models\User;
use yii\web\NotFoundHttpException;
use sadovojav\user\models\UserProfile;

/**
 * Class RegistrationController
 * @package sadovojav\user\controllers
 */
class RegistrationController extends \yii\web\Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['register'],
                'rules' => [
                    [
                        'actions' => ['register'],
                        'allow' => true,
                        'roles' => ['?']
                    ]
                ],
                'denyCallback' => function () {
                    return $this->goHome();
                }
            ],
        ];
    }

    public function actionRegister()
    {
        if (!$this->module->enableRegistration) {
            throw new NotFoundHttpException();
        }

        $user = new User();
        $user->setScenario('insert');

        $profile = new UserProfile();

        if ($user->load(Yii::$app->request->post()) && $profile->load(Yii::$app->request->post())) {
            if (Model::validateMultiple([$user, $profile])) {
                $user->save(false);

                $user->link('profile', $profile);

                $user->sendEmail();

                return $this->goHome();
            }
        }

        return $this->render('register', [
            'user' => $user,
            'profile' => $profile
        ]);
    }
}
