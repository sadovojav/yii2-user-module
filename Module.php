<?php

namespace sadovojav\user;

use Yii;
use yii\web\GroupUrlRule;
use yii\console\Application as ConsoleApplication;

/**
 * Class Module
 * @package sadovojav\user
 */
class Module extends \yii\base\Module
{
    public $controllerNamespace = 'sadovojav\user\controllers';

    /**
     * Access to control
     * @var array
     */
    public $access = ['@'];

    /**
     * Model map
     * @var array
     */
    public $modelMap = [];

    /**
     * Command controller namespace
     * @var string
     */
    public $commandControllerNamespace = 'sadovojav\user\commands';

    /**
     * Mail view path
     * @var string
     */
    public $mailViewPath = '@sadovojav/user/views/mail';

    /**
     * Enable registration
     * @var bool
     */
    public $enableRegistration = false;

    /**
     * Enable social autentification
     * @var bool
     */
    public $enableSocialAuth = false;

    /**
     * Password reset token expire
     * @var int
     */
    public $passwordResetTokenExpire = 43200;

    /**
     * The time you want the user will be remembered without asking for credentials
     * @var int
     */
    public $rememberFor = 1209600;

    /**
     * Captcha attempts
     * @var int
     */
    public $captchaAttempts = 3;

    private $_defaultModelMap = [
        'User' => 'sadovojav\user\model\User',
        'UserSearch' => 'sadovojav\user\model\UserSearch',
        'UserProfile' => 'sadovojav\user\model\UserProfile',
        'UserConfig' => 'sadovojav\user\model\UserConfig',
        'LoginForm' => 'sadovojav\user\model\LoginForm',
        'Query' => 'sadovojav\user\model\Query',
    ];

    public function init()
    {
        parent::init();

        $this->modules = [
            'admin' => [
                'class' => 'sadovojav\user\modules\admin\Module',
            ]
        ];

        $modelMap = array_merge($this->_defaultModelMap, $this->modelMap);

        foreach ($modelMap as $name => $definition) {
            $class = 'sadovojav\user\models' . $name;

            Yii::$container->set($class, $definition);
        }

        if (Yii::$app instanceof ConsoleApplication) {
            $this->controllerNamespace = $this->commandControllerNamespace;
        }
    }

    public static function t($category, $message, $params = [], $language = null)
    {
        return Yii::t('sadovojav/user/' . $category, $message, $params, $language);
    }
}
