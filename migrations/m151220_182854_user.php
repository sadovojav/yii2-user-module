<?php

use yii\db\Schema;
use yii\db\Migration;

/**
 * Class m151220_182854_user
 */
class m151220_182854_user extends Migration
{
    public function up()
    {
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        /**
         * Create {{%user}} table
         */
        $this->createTable('{{%user}}', [
            'id' => Schema::TYPE_PK,
            'username' => Schema::TYPE_STRING . '(32) NOT NULL',
            'email' => Schema::TYPE_STRING . '(32) NOT NULL',
            'auth_key' => Schema::TYPE_STRING . '(255) NOT NULL',
            'password' => Schema::TYPE_STRING . '(255) NOT NULL',
            'password_reset_token' => Schema::TYPE_STRING . '(255) NOT NULL',
            'role' => Schema::TYPE_STRING . '(32) NOT NULL',
            'status' => Schema::TYPE_SMALLINT . '(1) NOT NULL DEFAULT 1',
            'created_at' => Schema::TYPE_DATETIME . ' NOT NULL',
            'updated_at' => Schema::TYPE_DATETIME . ' NOT NULL',
        ], $tableOptions);

        $this->createIndex('uk_user_1', '{{%user}}', 'email', true);
        $this->createIndex('uk_user_2', '{{%user}}', 'username', true);
        $this->createIndex('idx_user_1', '{{%user}}', 'email, status');
        $this->createIndex('idx_user_2', '{{%user}}', 'username, status');
        $this->createIndex('idx_user_3', '{{%user}}', 'role');
        $this->createIndex('idx_user_4', '{{%user}}', 'status');

        /**
         * Create {{%user_profile}} table
         */
        $this->createTable('{{%user_profile}}', [
            'id' => Schema::TYPE_PK,
            'user_id' => Schema::TYPE_INTEGER . '(11) NOT NULL',
            'name_first' => Schema::TYPE_STRING . '(50) NOT NULL',
            'name_last' => Schema::TYPE_STRING . '(50) NOT NULL',
        ], $tableOptions);

        $this->createIndex('idx_user_profile_1', '{{%user_profile}}', 'name_first');
        $this->createIndex('idx_user_profile_2', '{{%user_profile}}', 'name_last');
        $this->addForeignKey('fk_user_profile_1', '{{%user_profile}}', 'user_id', '{{%user}}', 'id', 'CASCADE', 'RESTRICT');

        /**
         * Create {{%user_auth}} table
         */
        $this->createTable('{{%user_auth}}', [
            'id' => Schema::TYPE_PK,
            'user_id' => Schema::TYPE_INTEGER . '(11) NOT NULL',
            'source' => Schema::TYPE_STRING . '(255) NOT NULL',
            'source_id' => Schema::TYPE_STRING . '(255) NOT NULL',
        ], $tableOptions);

        $this->createIndex('idx_user_auth_1', '{{%user_auth}}', 'source');
        $this->createIndex('idx_user_auth_2', '{{%user_auth}}', 'source_id');
        $this->addForeignKey('fk_user_auth_1', '{{%user_auth}}', 'user_id', '{{%user}}', 'id', 'CASCADE', 'RESTRICT');

        $this->addDefaultUser();
    }

    public function down()
    {
        echo "m151220_182854_user cannot be reverted.\n";

        $this->dropTable('{{%user}}');
        $this->dropTable('{{%user_profile}}');
        $this->dropTable('{{%user_auth}}');

        return false;
    }

    private function addDefaultUser()
    {
        $faker = Faker\Factory::create();

        $this->insert('{{%user}}', [
            'username' => 'admin',
            'email' => $faker->freeEmail,
            'auth_key' => Yii::$app->security->generateRandomString(),
            'password' => Yii::$app->security->generatePasswordHash('admin'),
            'password_reset_token' => Yii::$app->security->generateRandomString() . '_' . time(),
            'role' => 'admin',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ]);

        $this->insert('{{%user_profile}}', [
            'user_id' => 1,
            'name_first' => $faker->firstName,
            'name_last' => $faker->lastName,
        ]);
    }
}
