<?php

return [
    'Users' => 'Пользователи',
    'User manager' => 'Управление пользователями',

    'Save' => 'Сохранить',
    'Create' => 'Создать',
    'Update' => 'Обновить',
    'Delete' => 'Удалить',
    'View' => 'Обзор',
    'Reset' => 'Сброс',

    'User Name' => 'Имя пользователя',
    'Name First' => 'Имя',
    'Name Last' => 'Фамилия',
    'Name Full' => 'Полное имя',
    'Email' => 'Email',
    'Auth key' => 'Auth Key',
    'Role' => 'Роль',
    'Password' => 'Пароль',
    'Password repeat' => 'Подтвержение пароля',
    'Created at' => 'Создан',
    'Updated at' => 'Обновлён',
    'Status' => 'Статус',

    'Admin' => 'Администратор',
    'User' => 'Пользователь',
    'Manager' => 'Менеджер',

    'Remember me' => 'Запомнить меня',

    'Delete item' => 'Вы действительно хотите удалить данную запись?',
    'Delete items' => 'Вы действительно хотите удалить данные записи?',

    'Send' => 'Отправить',
    'Registration' => 'Регистрация',
    'Sign in' => 'Вход',
    'Login' => 'Войти',
    'REQUEST RESET' => 'Запрос сброса',
    'Reset password' => 'Сброс пароля',
    'Do not have account? Sign up!' => 'Нет аккаунта? Зарегистрироваться!',
    'Password reset request' => 'Запрос на сброс пароля',
    'Password reset' => 'Сброс пароля',
    'Please choose your new password:' => 'Пожалуйста введите Ваш новый пароль',
    'Please fill out your email. A link to reset password will be sent there' => 'Пожалуйста введите Ваш email. Ссылка на сброс пароля будет отправленна на этот адрес'
];