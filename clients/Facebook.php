<?php

namespace sadovojav\user\clients;

/**
 * Class Facebook
 * @package sadovojav\user\clients
 */
class Facebook extends \yii\authclient\clients\Facebook implements ClientInterface
{
    public function getEmail()
    {
        return isset($this->getUserAttributes()['email']) ? $this->getUserAttributes()['email'] : null;
    }

    public function getUsername()
    {
        return;
    }
}
