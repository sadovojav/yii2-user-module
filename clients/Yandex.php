<?php

namespace sadovojav\user\clients;

use sadovojav\user\Module;

/**
 * Class Yandex
 * @package sadovojav\user\clients
 */
class Yandex extends \yii\authclient\clients\Yandex implements ClientInterface
{
    public function getEmail()
    {
        $emails = isset($this->getUserAttributes()['emails']) ? $this->getUserAttributes()['emails'] : null;

        if ($emails !== null && isset($emails[0])) {
            return $emails[0];
        } else {
            return null;
        }
    }

    public function getUsername()
    {
        return isset($this->getUserAttributes()['login']) ? $this->getUserAttributes()['login'] : null;
    }

    protected function defaultTitle()
    {
        return Module::t('user', 'Yandex');
    }
}
