<?php

namespace sadovojav\user\clients;

/**
 * Interface ClientInterface
 * @package sadovojav\user\clients
 */
interface ClientInterface extends \yii\authclient\ClientInterface
{
    public function getEmail();

    public function getUsername();
}
