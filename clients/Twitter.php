<?php

namespace sadovojav\user\clients;

use yii\helpers\ArrayHelper;

/**
 * Class Twitter
 * @package sadovojav\user\clients
 */
class Twitter extends \yii\authclient\clients\Twitter implements ClientInterface
{
    public function getUsername()
    {
        return ArrayHelper::getValue($this->getUserAttributes(), 'screen_name');
    }

    public function getEmail()
    {
        return null;
    }
}
