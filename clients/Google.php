<?php

namespace sadovojav\user\clients;

/**
 * Class Google
 * @package sadovojav\user\clients
 */
class Google extends \yii\authclient\clients\Google implements ClientInterface
{
    public function getEmail()
    {
        return isset($this->getUserAttributes()['email']) ? $this->getUserAttributes()['email'] : null;
    }

    public function getUsername()
    {
        return;
    }
}
