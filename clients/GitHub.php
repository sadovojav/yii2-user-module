<?php

namespace sadovojav\user\clients;

/**
 * Class GitHub
 * @package sadovojav\user\clients
 */
class GitHub extends \yii\authclient\clients\GitHub implements ClientInterface
{
    public function getEmail()
    {
        return isset($this->getUserAttributes()['email']) ? $this->getUserAttributes()['email'] : null;
    }

    public function getUsername()
    {
        return isset($this->getUserAttributes()['login']) ? $this->getUserAttributes()['login'] : null;
    }
}
