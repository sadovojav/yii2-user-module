<?php

namespace sadovojav\user\clients;

use sadovojav\user\Module;

/**
 * Class VKontakte
 * @package sadovojav\user\clients
 */
class VKontakte extends \yii\authclient\clients\VKontakte  implements ClientInterface
{
    public $scope = 'email';

    public function getEmail()
    {
        return $this->getAccessToken()->getParam('email');
    }

    public function getUsername()
    {
        return isset($this->getUserAttributes()['screen_name']) ? $this->getUserAttributes()['screen_name'] : null;
    }

    protected function defaultTitle()
    {
        return Module::t('user', 'VKontakte');
    }
}
