<?php

namespace sadovojav\user;

use yii\i18n\PhpMessageSource;

/**
 * Class Bootstrap
 * @package sadovojav\user
 */
class Bootstrap implements \yii\base\BootstrapInterface
{
    public function bootstrap($app)
    {
        $app->i18n->translations['sadovojav/user/*'] = [
            'class' => PhpMessageSource::className(),
            'sourceLanguage' => 'en-US',
            'forceTranslation' => true,
            'basePath' => '@vendor/sadovojav/yii2-user-module/messages',
            'fileMap' => [
                'sadovojav/user/user' => 'user.php',
            ],
        ];
    }
}
