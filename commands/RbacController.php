<?php

namespace sadovojav\user\commands;

use Yii;
use yii\rbac\DbManager;
use sadovojav\user\components\rbac\UserRoleRule;
use sadovojav\user\components\rbac\UserProfileOwnerRule;

/**
 * Class RbacController
 * @package console\controllers
 */
class RbacController extends \yii\console\Controller
{
    public function actionInit()
    {
        $auth = new DbManager;
        $auth->init();

        $auth->removeAll();

        $rule = new UserRoleRule;
        $auth->add($rule);

        // User role
        $userRole = $auth->createRole('user');
        $userRole->ruleName = $rule->name;
        $userRole->description = 'User';
        $auth->add($userRole);

        // Manager role
        $managerRole = $auth->createRole('manager');
        $managerRole->ruleName = $rule->name;
        $managerRole->description = 'Manager';
        $auth->add($managerRole);

        // Administrator role
        $adminRole = $auth->createRole('admin');
        $adminRole->ruleName = $rule->name;
        $adminRole->description = 'Administrator';
        $auth->add($adminRole);

        // Create simple, based on action permissions
        $indexPermission = $auth->createPermission('index');
        $viewPermission = $auth->createPermission('view');
        $createPermission = $auth->createPermission('create');
        $updatePermission = $auth->createPermission('update');
        $deletePermission = $auth->createPermission('delete');
        $backendPermission = $auth->createPermission('backend');

        // Add permissions in Yii::$app->authManager
        $auth->add($indexPermission);
        $auth->add($viewPermission);
        $auth->add($createPermission);
        $auth->add($updatePermission);
        $auth->add($deletePermission);
        $auth->add($backendPermission);

        // Create user profile owner rule
        $userProfileOwnerRule = new UserProfileOwnerRule();
        $auth->add($userProfileOwnerRule);
        $updateOwnProfile = $auth->createPermission('updateOwnProfile');
        $updateOwnProfile->ruleName = $userProfileOwnerRule->name;
        $auth->add($updateOwnProfile);

        // Add child to Manager
        $auth->addChild($managerRole, $userRole);
        $auth->addChild($managerRole, $indexPermission);
        $auth->addChild($managerRole, $viewPermission);
        $auth->addChild($managerRole, $createPermission);
        $auth->addChild($managerRole, $updatePermission);
        $auth->addChild($managerRole, $deletePermission);
        $auth->addChild($managerRole, $backendPermission);
        $auth->addChild($managerRole, $updateOwnProfile);

        // Add child to Admin
        $auth->addChild($adminRole, $managerRole);
    }
}