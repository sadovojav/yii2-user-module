<?php

namespace sadovojav\user\widgets;

use sadovojav\user\models\forms\LoginForm;

/**
 * Class Login
 * @package sadovojav\user\widgets
 */
class Login extends \yii\base\Widget
{
    /**
     * Widget view
     * @var
     */
    public $view = 'login';

    public function run()
    {
        $model = new LoginForm();

        return $this->render($this->view, [
            'model' => $model
        ]);
    }
}
