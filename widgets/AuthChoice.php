<?php

namespace sadovojav\user\widgets;

use Yii;
use yii\helpers\Url;
use yii\helpers\Html;
use sadovojav\user\Module;
use yii\authclient\ClientInterface;
use yii\authclient\widgets\AuthChoiceAsset;

/**
 * Class AuthChoice
 * @package sadovojav\user\widgets
 */
class AuthChoice extends \yii\authclient\widgets\AuthChoice
{
    /**
     * Acounts
     * @var
     */
    public $accounts;

    /**
     * Options
     * @var array
     */
    public $options = [];

    public function init()
    {
        if (Module::getInstance()->enableSocialAuth) {
            AuthChoiceAsset::register(Yii::$app->view);

            if ($this->popupMode) {
                Yii::$app->view->registerJs("\$('#" . $this->getId() . "').authchoice();");
            }

            $this->options['id'] = $this->getId();

            echo Html::beginTag('div', $this->options);
        }
    }


    public function createClientUrl($provider)
    {
        if ($this->isConnected($provider)) {
            return Url::to(['/user/settings/disconnect', 'id' => $this->accounts[$provider->getId()]->id]);
        } else {
            return parent::createClientUrl($provider);
        }
    }

    /**
     * Checks if provider already connected to user.
     * @param ClientInterface $provider
     * @return bool
     */
    public function isConnected(ClientInterface $provider)
    {
        return $this->accounts != null && isset($this->accounts[$provider->getId()]);
    }
}
