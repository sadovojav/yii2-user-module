<?php

namespace sadovojav\user\widgets;

use sadovojav\user\models\User;

/**
 * Class Registration
 * @package sadovojav\user\widgets
 */
class Registration extends \yii\base\Widget
{
    /**
     * Widget view
     * @var
     */
    public $view = 'registration';

    public function run()
    {
        $model = new User();
        $model->setScenario('insert');

        return $this->render($this->view, [
            'model' => $model
        ]);
    }
}
