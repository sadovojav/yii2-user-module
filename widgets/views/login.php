<?php

use yii\helpers\Html;
use sadovojav\user\Module;
use yii\bootstrap\ActiveForm;

?>

<div class="user user-widget login">
    <?php $form = ActiveForm::begin([
        'id' => 'login-form',
        'enableClientValidation' => false
    ]); ?>

    <?= $form->field($model, 'username')->textInput(); ?>

    <?= $form->field($model, 'password')->passwordInput(); ?>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'rememberMe')->checkbox() ?>
        </div>

        <div class="col-md-6">
            <?= Html::submitButton(Module::t('user', 'Login'), [
                'class' => 'login-btn'
            ]); ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

    <?= Html::a(Module::t('user', 'Reset password'), ['/user/password/request-reset']); ?>
</div>
