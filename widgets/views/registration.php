<?php

use yii\helpers\Html;
use sadovojav\user\Module;
use yii\bootstrap\ActiveForm;

?>

<div class="user user-widget registration">
    <?php $form = ActiveForm::begin([
        'id' => 'registration-form',
        'enableClientValidation' => false
    ]); ?>

    <?= $form->field($model, 'username')->textInput([
        'maxlength' => 32,
    ]); ?>

    <?= $form->field($model, 'email')->textInput([
        'maxlength' => 32
    ]); ?>

    <?= $form->field($model, 'password')->passwordInput([
        'maxlength' => 255,
        'value' => ''
    ]); ?>

    <?= $form->field($model, 'passwordRepeat')->passwordInput([
        'maxlength' => 255,
        'value' => ''
    ]); ?>

    <?= Html::submitButton(Module::t('user', 'Create'), [
        'class' => 'registration-btn'
    ]); ?>

    <?php ActiveForm::end(); ?>
</div>
