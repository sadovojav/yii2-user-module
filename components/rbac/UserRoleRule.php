<?php

namespace sadovojav\user\components\rbac;

use Yii;
use yii\rbac\Rule;
use yii\helpers\ArrayHelper;
use sadovojav\user\models\User;

/**
 * Class UserRoleRule
 * @package sadovojav\user\components\rbac
 */
class UserRoleRule extends Rule
{
    public $name = 'userRole';

    public function execute($user, $item, $params)
    {
        $user = ArrayHelper::getValue($params, 'user', User::findOne($user));

        if ($user) {
            $role = $user->role;

            if ($item->name === User::ROLE_ADMIN) {
                return $role == User::ROLE_ADMIN;
            } elseif ($item->name === User::ROLE_MANAGER) {
                return $role == User::ROLE_ADMIN || $role == User::ROLE_MANAGER;
            } elseif ($item->name === User::ROLE_USER) {
                return $role == User::ROLE_ADMIN || $role == User::ROLE_MANAGER || $role == User::ROLE_USER;
            }
        }

        return false;
    }
}