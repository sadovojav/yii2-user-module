<?php

namespace sadovojav\user\components\rbac;

use Yii;
use yii\rbac\Rule;
use sadovojav\user\models\User;

/**
 * Class UserProfileOwnerRule
 * @package sadovojav\user\components\rbac
 */
class UserProfileOwnerRule extends Rule
{
    public $name = 'isProfileOwner';

    public function execute($user, $item, $params)
    {
        if (Yii::$app->user->identity->role == User::ROLE_ADMIN) {
            return true;
        }

        return isset($params['profileId']) ? Yii::$app->user->id == $params['profileId'] : false;
    }
}