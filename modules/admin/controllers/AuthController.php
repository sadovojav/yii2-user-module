<?php

namespace sadovojav\user\modules\admin\controllers;

use Yii;
use yii\filters\AccessControl;
use sadovojav\user\Module;
use sadovojav\user\modules\admin\models\LoginForm;

/**
 * Class AuthController
 * @package sadovojav\user\controllers
 */
class AuthController extends \yii\web\Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['login', 'logout', 'captcha'],
                'rules' => [
                    [
                        'actions' => ['login', 'captcha'],
                        'allow' => true,
                        'roles' => ['?']
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@']
                    ],
                ],
                'denyCallback' => function () {
                    return $this->goHome();
                }
            ],
        ];
    }

    public function actions()
    {
        return [
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'testLimit' => 1
            ],
        ];
    }

    public function actionLogin()
    {
        $model = new LoginForm();

        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            unset(Yii::$app->session['captchaAttempts']);

            return $this->goBack();
        } else {
            $this->layout = 'login';

            if (isset(Yii::$app->session['captchaAttempts'])) {
                $count = Yii::$app->session['captchaAttempts'] + 1;

                Yii::$app->session['captchaAttempts'] = $count;
            } else {
                Yii::$app->session['captchaAttempts'] = 0;
            }

            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }
}
