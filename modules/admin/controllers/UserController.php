<?php

namespace sadovojav\user\modules\admin\controllers;

use Yii;
use yii\base\Model;
use yii\web\Response;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\NotFoundHttpException;
use yii\web\ForbiddenHttpException;
use sadovojav\user\models\User;
use yii\web\BadRequestHttpException;
use sadovojav\user\models\UserSearch;
use sadovojav\user\models\UserConfig;
use sadovojav\user\models\UserProfile;

/**
 * Class UserController
 * @package backend\controllers
 */
class UserController extends \yii\web\Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index', 'view', 'create', 'update', 'delete', 'delete-multiple', 'change-status'],
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['backend']
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'delete-multiple' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel
        ]);
    }

    /**
     * Displays a single User model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $user = new User();
        $user->setScenario('insert');

        $profile = new UserProfile();

        if ($user->load(Yii::$app->request->post()) && $profile->load(Yii::$app->request->post())) {
            if (Model::validateMultiple([$user, $profile])) {
                $user->save(false);

                $user->link('profile', $profile);

                return $this->redirect(['view', 'id' => $user->id]);
            }
        }

        return $this->render('create', [
            'user' => $user,
            'profile' => $profile
        ]);
    }

    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        if (!Yii::$app->user->can('updateOwnProfile', ['profileId' => $id])) {
            throw new ForbiddenHttpException;
        }

        $user = $this->findModel($id);

        if ($user->load(Yii::$app->request->post()) && $user->profile->load(Yii::$app->request->post())) {
            if (Model::validateMultiple([$user, $user->profile])) {
                $user->save(false);

                $user->link('profile', $user->profile);

                return $this->redirect(['view', 'id' => $user->id]);
            }
        }

        return $this->render('create', [
            'user' => $user,
            'profile' => $user->profile
        ]);
    }

    /**
     * Deletes an existing User model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        if (Yii::$app->user->id == $id) {
            throw new ForbiddenHttpException;
        }

        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Deletes an existing User models.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @return mixed
     */
    public function actionDeleteMultiple()
    {
        $pks = Yii::$app->request->post('pks');

        if (!$pks) {
            return false;
        }

        return User::deleteAll(['id' => $pks]);
    }

    /**
     * Cache status to User models.
     * @return bool|int
     */
    public function actionChangeStatus()
    {
        $pks = Yii::$app->request->post('pks');

        if (!$pks) {
            return false;
        }

        return User::updateAll(['status' => Yii::$app->request->post('status')], ['id' => $pks]);
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
