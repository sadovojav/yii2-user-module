<?php

namespace sadovojav\user\modules\admin;

use Yii;

/**
 * Class Module
 * @package sadovojav\user\modules\admin
 */
class Module extends \yii\base\Module
{
    public $controllerNamespace = 'sadovojav\user\modules\admin\controllers';

    public function init()
    {
        parent::init();
    }
}
