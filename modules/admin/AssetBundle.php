<?php

namespace sadovojav\user\modules\admin;

use yii\web\View;

/**
 * Class AssetBundle
 * @package sadovojav\user\modules\admin
 */
class AssetBundle extends \yii\web\AssetBundle
{
    public $js = [
        'js/scripts.js'
    ];

    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];

    public function init()
    {
        $this->sourcePath = __DIR__ . '/assets';
        $this->jsOptions['position'] = View::POS_HEAD;

        parent::init();
    }
}
