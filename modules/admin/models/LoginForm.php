<?php

namespace sadovojav\user\modules\admin\models;

use Yii;
use sadovojav\user\Module;
use sadovojav\user\models\User;

/**
 * Class LoginForm
 * @package backend\models
 */
class LoginForm extends \yii\base\Model
{
    public $username;
    public $password;
    public $captcha;
    public $rememberMe = true;

    private $user = false;

    public function rules()
    {
        return [
            [['username', 'password'], 'required'],
            [['username', 'password', 'captcha'], 'required', 'when' => function () {
                return $this->checkAttempts();
            }],
            ['captcha', 'captcha', 'captchaAction' => '/user/admin/auth/captcha', 'when' => function () {
                return $this->checkAttempts();
            }],
            ['rememberMe', 'boolean'],
            ['password', 'validatePassword'],
        ];
    }

    public function checkAttempts()
    {
        return Yii::$app->session['captchaAttempts'] >= Module::getInstance()->captchaAttempts;
    }

    public function attributeLabels()
    {
        return [
            'username' => Module::t('user', 'User Name'),
            'password' => Module::t('user', 'Password'),
            'rememberMe' => Module::t('user', 'Remember me'),
        ];
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();

            if (!$user || !$user->validatePassword($this->password)) {
                $this->addError($attribute, 'Incorrect username or password.');
            }
        }
    }

    /**
     * Logs in a user using the provided username and password.
     * @return boolean whether the user is logged in successfully
     */
    public function login()
    {
        if ($this->validate()) {
            return Yii::$app->user->login($this->getUser(), $this->rememberMe ? 3600 * 24 * 30 : 0);
        } else {
            return false;
        }
    }

    /**
     * Finds user by [[username]]
     * @return User|null
     */
    public function getUser()
    {
        if ($this->user === false) {
            $attribute = strpos($this->username, '@') ? 'email' : 'username';

            $this->user = User::find()->where($attribute . ' = :login AND status = :status', [
                ':login' => $this->username,
                ':status' => User::STATUS_ACTIVE
            ])->one();

            if (!Yii::$app->user->can('backend', ['user' => $this->user])) {
                $this->user = null;
            }
        }

        return $this->user;
    }
}
