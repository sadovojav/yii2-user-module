$("body").on("click", "[data-action=delete-multiple]", function () {
    var pks = $("#w0").yiiGridView("getSelectedRows");

    if (!pks.length) {
        return false;
    }

    if (confirm($(this).data("title"))) {
        $.post(
            $(this).data("url"), {
                pks: pks
            },
            function () {
                $.pjax.reload({container: "#w0-container"});
            }
        );
    }

    return false;
});

$("body").on("click", "[data-action=change-status]", function () {
    var pks = $("#w0").yiiGridView("getSelectedRows");

    if (!pks.length) {
        return false;
    }

    $.post(
        $(this).data("url"), {
            pks: pks,
            status: $(this).data("status")
        },
        function () {
            $.pjax.reload({container: "#w0-container"});
        }
    );

    return false;
});