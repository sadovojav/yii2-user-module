<?php

use sadovojav\user\Module;

$this->title = Module::t('user', 'Create');
$this->params['breadcrumbs'] = [
    ['label' => Module::t('user', 'Users'), 'url' => ['index']],
    $this->title,
];

?>

<div class="user-create">
    <?= $this->render('_form', [
        'user' => $user,
        'profile' => $profile,
    ]); ?>
</div>
