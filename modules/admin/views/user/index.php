<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use sadovojav\user\Module;
use sadovojav\user\models\User;
use sadovojav\user\modules\admin\AssetBundle;

AssetBundle::register($this);

$this->title = Module::t('user', 'User manager');
$this->params['breadcrumbs'] = [
    $this->title,
];

?>

<div class="user-index">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'panel' => [
            'type' => GridView::TYPE_DEFAULT,
            'heading' => false
        ],
        'toolbar' => [
            ['content' =>
                Html::a('<i class="glyphicon glyphicon-plus"></i>', ['create'], [
                    'title' => Module::t('user', 'Create'),
                    'class' => 'btn btn-box-tool'
                ]) . ' ' .
                Html::a('<i class="glyphicon glyphicon-repeat"></i>', [
                    'index'
                ], [
                    'data-pjax' => 0,
                    'title' => Module::t('user', 'Reset'),
                    'class' => 'btn btn-box-tool',
                ]) . ' ' .
                Html::a('<i class="glyphicon glyphicon-ok-circle"></i>', [
                    'index'
                ], [
                    'title' => Module::t('user', 'Active'),
                    'class' => 'btn btn-box-tool',
                    'data-action' => 'change-status',
                    'data-status' => 1,
                    'data-url' => Yii::$app->urlManager->createUrl(['/user/change-status'])
                ]) . ' ' .
                Html::a('<i class="glyphicon glyphicon-remove-circle"></i>', [
                    'index'
                ], [
                    'title' => Module::t('user', 'Inactive'),
                    'class' => 'btn btn-box-tool',
                    'data-action' => 'change-status',
                    'data-status' => 0,
                    'data-url' => Yii::$app->urlManager->createUrl(['/user/change-status'])
                ]) . ' ' .
                Html::a('<i class="glyphicon glyphicon-trash"></i>', [
                    'index'
                ], [
                    'title' => Module::t('user', 'Delete'),
                    'class' => 'btn btn-box-tool',
                    'data-action' => 'delete-multiple',
                    'data-title' => Module::t('user', 'Delete items'),
                    'data-url' => Yii::$app->urlManager->createUrl(['/user/delete-multiple'])
                ])
            ],
//            '{toggleData}'
        ],
        'export' => false,
        'pjax' => true,
        'condensed' => true,
        'bordered' => true,
        'columns' => [
            ['class' => 'kartik\grid\SerialColumn'],
            [
                'attribute' => 'id',
                'width' => '75px'
            ],
            'username',
//            'email',
//			'auth_key',
//			'password',
            [
                'attribute' => 'role',
                'value' => 'roleName',
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => User::getRoles(),
                'filterWidgetOptions' => [
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ],
                'filterInputOptions' => [
                    'placeholder' => '-'
                ],
            ],
            [
                'class' => 'kartik\grid\BooleanColumn',
                'attribute' => 'status',
                'vAlign' => 'middle',
            ],
//			'created_at',
//			'updated_at',
            [
                'class' => 'kartik\grid\ActionColumn',
                'vAlign' => 'middle',
            ],
            [
                'class' => 'kartik\grid\CheckboxColumn',
                'headerOptions' => [
                    'class' => 'kartik-sheet-style'
                ],
            ],
        ],
    ]); ?>
</div>
