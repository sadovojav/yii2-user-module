<?php

use sadovojav\user\Module;

$this->title = $user->username;
$this->params['breadcrumbs'] = [
    ['label' => Module::t('user', 'Users'), 'url' => ['index']],
    ['label' => $user->username, 'url' => ['view', 'id' => $user->id]],
    Module::t('user', 'Update'),
];

?>

<div class="user-update">
    <?= $this->render('_form', [
        'user' => $user,
        'profile' => $profile
    ]); ?>
</div>
