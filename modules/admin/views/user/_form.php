<?php

use yii\helpers\Html;
use sadovojav\user\Module;
use yii\widgets\ActiveForm;
use kartik\widgets\Select2;
use sadovojav\user\models\User;
use kartik\widgets\SwitchInput;

?>

<div class="user-form">
    <?php $form = ActiveForm::begin([
        'options' => [
            'enctype' => 'multipart/form-data'
        ]
    ]); ?>

    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title"><?= $user->username; ?></h3>

            <div class="box-tools pull-right">
                <?= Html::a('<i class="glyphicon glyphicon-plus"></i>', ['create'], [
                    'title' => Module::t('user', 'Create'),
                    'class' => 'btn btn-box-tool'
                ]); ?>
                <?= Html::submitButton('<i class="glyphicon glyphicon-floppy-disk"></i>', [
                    'title' => Module::t('user', 'Save'),
                    'class' => 'btn btn-box-tool',
                ]); ?>
                <?php if (!$user->isNewRecord) : ?>
                    <?= Html::a('<i class="glyphicon glyphicon-trash"></i>', ['delete', 'id' => $user->id], [
                        'title' => Module::t('user', 'Delete'),
                        'class' => 'btn btn-box-tool',
                        'data' => [
                            'confirm' => Module::t('user', 'Delete item'),
                            'method' => 'post',
                        ],
                    ]); ?>
                <?php endif; ?>
            </div>
        </div>

        <div class="box-body">
            <div class="row">
                <div class="col-md-6">
                    <?= $form->field($user, 'username')->textInput([
                        'maxlength' => 32,
                        'disabled' => !$user->isNewRecord
                    ]); ?>

                    <div class="row">
                        <div class="col-sm-6">
                            <?= $form->field($profile, 'name_first')->textInput([
                                'maxlength' => 50
                            ]); ?>
                        </div>

                        <div class="col-sm-6">
                            <?= $form->field($profile, 'name_last')->textInput([
                                'maxlength' => 50
                            ]); ?>
                        </div>
                    </div>

                    <?= $form->field($user, 'email')->textInput([
                        'maxlength' => 32
                    ]); ?>

                    <?= $form->field($user, 'role')->widget(Select2::classname(), [
                        'data' => User::getRoles(),
                        'options' => [
                            'placeholder' => ''
                        ],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ]); ?>
                </div>

                <div class="col-md-6">
                    <?= $form->field($user, 'auth_key')->textInput([
                        'maxlength' => 255,
                        'disabled' => !$user->isNewRecord
                    ]); ?>

                    <?= $form->field($user, 'password')->passwordInput([
                        'maxlength' => 255,
                        'value' => ''
                    ]); ?>

                    <?= $form->field($user, 'passwordRepeat')->passwordInput([
                        'maxlength' => 255,
                        'value' => ''
                    ]); ?>

                    <?= $form->field($user, 'status')->widget(SwitchInput::className(), [
                        'pluginOptions' => [
                            'size' => 'small',
                            'onColor' => 'success',
                            'offColor' => 'danger',
                        ]
                    ]); ?>
                </div>
            </div>
        </div>

        <div class="box-footer">
            <div class="text-right">
                <?= Html::submitButton(Module::t('user', 'Save'), [
                    'class' => 'btn btn-primary btn-sm'
                ]); ?>
            </div>
        </div>
    </div>

    <?php ActiveForm::end(); ?>
</div>
