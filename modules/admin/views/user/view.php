<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use sadovojav\user\Module;

$this->title = Module::t('user', 'View');
$this->params['breadcrumbs'] = [
    ['label' => Module::t('user', 'Users'), 'url' => ['index']],
    $this->title,
];

?>

<div class="user-view">
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title"><?= $model->username; ?></h3>

            <div class="box-tools pull-right">
                <?= Html::a('<i class="glyphicon glyphicon-plus"></i>', ['create'], [
                    'title' => Module::t('user', 'Create'),
                    'class' => 'btn btn-box-tool'
                ]); ?>
                <?= Html::a('<i class="glyphicon glyphicon-pencil"></i>', ['update', 'id' => $model->id], [
                    'title' => Module::t('user', 'Update'),
                    'class' => 'btn btn-box-tool'
                ]); ?>
                <?= Html::a('<i class="glyphicon glyphicon-trash"></i>', ['delete', 'id' => $model->id], [
                    'title' => Module::t('user', 'Delete'),
                    'class' => 'btn btn-box-tool',
                    'data' => [
                        'confirm' => Module::t('user', 'Are you sure you want to delete this items?'),
                        'method' => 'post',
                    ],
                ]); ?>
            </div>
        </div>

        <div class="box-body">
            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'id',
                    'username',
                    'profile.name_first',
                    'profile.name_last',
                    'email:email',
//                    'auth_key',
//                    'password',
                    [
                        'attribute' => 'role',
                        'value' => $model->roleName
                    ],
                    'status:boolean',
                    'created_at',
                    'updated_at',
                ],
            ]); ?>
        </div>
    </div>
</div>
