<?php

use yii\helpers\Html;
use yii\captcha\Captcha;
use sadovojav\user\Module;
use yii\bootstrap\ActiveForm;

$this->title = Module::t('user', 'Sign in');

$usernameOptions = [
    'options' => ['class' => 'form-group has-feedback'],
    'inputTemplate' => "{input}<span class='glyphicon glyphicon-envelope form-control-feedback'></span>"
];

$passwordOptions = [
    'options' => ['class' => 'form-group has-feedback'],
    'inputTemplate' => "{input}<span class='glyphicon glyphicon-lock form-control-feedback'></span>"
];
?>

<div class="login-box">
    <div class="login-logo">
        <a href="/backend"><b>Admin</b>Panel</a>
    </div>

    <div class="login-box-body">
        <p class="login-box-msg">Sign in to start your session</p>

        <?php $form = ActiveForm::begin([
            'id' => 'login-form',
            'enableClientValidation' => false
        ]); ?>

        <?= $form->field($model, 'username', $usernameOptions)->textInput([
            'autofocus' => true,
            'placeholder' => $model->getAttributeLabel('username')
        ])->label(false); ?>

        <?= $form->field($model, 'password', $passwordOptions)->passwordInput([
            'placeholder' => $model->getAttributeLabel('password')
        ])->label(false); ?>

        <?php if ($model->checkAttempts()) : ?>
            <?= $form->field($model, 'captcha')->widget(Captcha::className(), [
                'captchaAction' => ['/user/admin/auth/captcha'],
                'imageOptions' => [
                    'height' => 34,
                    'style' => 'cursor:pointer'
                ],
                'template' => '<div class="row"><div class="col-md-4">{image}</div><div class="col-md-8">{input}</div></div>'
            ])->label(false) ?>
        <?php endif; ?>

        <div class="row">
            <div class="col-xs-8">
                <?= $form->field($model, 'rememberMe')->checkbox() ?>
            </div>

            <div class="col-xs-4">
                <?= Html::submitButton(Module::t('user', 'Login'), [
                    'class' => 'btn btn-primary btn-block btn-flat',
                    'name' => 'login-button'
                ]); ?>
            </div>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
</div>
