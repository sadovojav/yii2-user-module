<?php

namespace sadovojav\user\models;

use Yii;
use yii\db\Expression;
use sadovojav\user\Module;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;

/**
 * User model
 *
 * @property integer $id
 * @property string $username
 * @property string $email
 * @property string $auth_key
 * @property string $password
 * @property string $password_reset_token
 * @property string $role
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property UserProfile $profile
 */
class User extends \yii\db\ActiveRecord implements \yii\web\IdentityInterface
{
    public $passwordRepeat;

    public static $usernameRegexp = '/^[a-zA-Z0-9_]+$/';

    const ROLE_ADMIN = 'admin';
    const ROLE_USER = 'user';
    const ROLE_MANAGER = 'manager';

    const STATUS_INACTIVE = 0;
    const STATUS_ACTIVE = 1;

    public static function tableName()
    {
        return '{{%user}}';
    }

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
            ]
        ];
    }

    public function rules()
    {
        return [
            // username rules
            ['username', 'required', 'on' => ['insert', 'connect', 'update']],
            ['username', 'match', 'pattern' => static::$usernameRegexp],
            ['username', 'trim'],
            ['username', 'string', 'min' => 3, 'max' => 32],
            ['username', 'unique', 'message' => Module::t('user', 'This username has already been taken')],
            // email rules
            ['email', 'required', 'on' => ['insert', 'connect', 'update']],
            ['email', 'trim'],
            ['email', 'email'],
            ['email', 'string', 'max' => 32],
            ['email', 'unique', 'message' => Module::t('user', 'This email address has already been taken')],
            // password rules
            ['password', 'required', 'on' => ['insert']],
            ['password', 'string', 'min' => 6, 'max' => 255, 'on' => ['insert']],
            ['password', 'compare', 'compareAttribute' => 'passwordRepeat', 'on' => ['insert', 'update']],
            ['password', 'filter', 'filter' => function ($value) {
                return empty($value)
                    ? $this->getOldAttribute('password')
                    : Yii::$app->security->generatePasswordHash($value);
            }],
            // role rules
            ['role', 'string', 'max' => 32],
            ['role', 'default', 'value' => self::ROLE_USER],
            // auth_key rules
            ['auth_key', 'string', 'max' => 255],
            ['auth_key', 'default', 'value' => Yii::$app->security->generateRandomString()],
            // status rules
            ['status', 'default', 'value' => self::STATUS_ACTIVE],
            // safe attributes
            [['passwordRepeat', 'created_at', 'updated_at'], 'safe']
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => Module::t('user', 'Id'),
            'username' => Module::t('user', 'User Name'),
            'nameFull' => Module::t('user', 'Name Full'),
            'email' => Module::t('user', 'Email'),
            'auth_key' => Module::t('user', 'Auth key'),
            'password' => Module::t('user', 'Password'),
            'password_reset_token' => Module::t('user', 'Password reset token'),
            'passwordRepeat' => Module::t('user', 'Password repeat'),
            'role' => Module::t('user', 'Role'),
            'status' => Module::t('user', 'Status'),
            'created_at' => Module::t('user', 'Created at'),
            'updated_at' => Module::t('user', 'Updated at'),
        ];
    }

    public static function find()
    {
        return new Query(get_called_class());
    }

    public function getProfile()
    {
        return $this->hasOne(UserProfile::className(), ['user_id' => 'id']);
    }

    public function getNameFull()
    {
        return trim($this->profile->name_first . ' ' . $this->profile->name_last);
    }

    public static function getRoles()
    {
        return [
            self::ROLE_ADMIN => Module::t('user', 'Admin'),
            self::ROLE_USER => Module::t('user', 'User'),
            self::ROLE_MANAGER => Module::t('user', 'Manager'),
        ];
    }

    public function getRoleName()
    {
        return self::getRoles()[$this->role];
    }

    public static function findIdentity($id)
    {
        return static::findOne([
            'id' => $id,
            'status' => self::STATUS_ACTIVE
        ]);
    }

    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * Finds user by username
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::findOne([
            'username' => $username,
            'status' => self::STATUS_ACTIVE
        ]);
    }

    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * Finds user by password reset token
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
            'password_reset_token' => $token,
            'status' => self::STATUS_ACTIVE,
        ]);
    }

    /**
     * Finds out if password reset token is valid
     * @param string $token password reset token
     * @return boolean
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }

        $timestamp = (int)substr($token, strrpos($token, '_') + 1);

        $expire = Module::getInstance()->passwordResetTokenExpire;

        return $timestamp + $expire >= time();
    }

    public function getAuthKey()
    {
        return $this->auth_key;
    }

    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     * @param string $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password);
    }

    /**
     * Generates password hash from password and sets it to the model
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }

    /**
     * Sends an email with a welcome message.
     * @return boolean whether the email was send
     */
    public function sendEmail()
    {
        $view = [
            'html' => 'html/welcome',
            'text' => 'text/welcome'
        ];

        $mailer = Yii::$app->mailer;
        $mailer->viewPath = Module::getInstance()->mailViewPath;

        return Yii::$app->mailer->compose($view, ['user' => $this])
            ->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->name . ' robot'])
            ->setTo($this->email)
            ->setSubject('Wellcome for ' . Yii::$app->name)
            ->send();
    }
}
