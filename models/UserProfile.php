<?php

namespace sadovojav\user\models;

use Yii;
use sadovojav\user\Module;

/**
 * UserProfile model
 *
 * @property integer $user_id
 * @property string $name_first
 * @property string $name_last
 */
class UserProfile extends \yii\db\ActiveRecord
{
    public static function tableName()
    {
        return '{{%user_profile}}';
    }

    public function rules()
    {
        return [
            // name_first rules
            ['name_first', 'string', 'max' => 50],
            ['name_first', 'required', 'on' => ['update']],
            // name_last rules
            ['name_last', 'string', 'max' => 50],
        ];
    }

    public function attributeLabels()
    {
        return [
            'user_id' => Module::t('user', 'Id'),
            'name_first' => Module::t('user', 'Name First'),
            'name_last' => Module::t('user', 'Name Last'),
        ];
    }
}