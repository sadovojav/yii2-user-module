<?php

namespace sadovojav\user\models;

/**
 * Class Query
 * @package sadovojav\user\models
 */
class Query extends \yii\db\ActiveQuery
{
    /**
     * @param bool $state
     * @return $this
     */
    public function active($state = true)
    {
        $this->andWhere(['status' => $state]);

        return $this;
    }
} 