<?php

namespace sadovojav\user\models;

/**
 * UserAuth model
 *
 * @property integer $id
 * @property string $user_id
 * @property string $source
 * @property string $source_id
 */
class UserAuth extends \yii\db\ActiveRecord
{
    public static function tableName()
    {
        return '{{%user_auth}}';
    }

    public function rules()
    {
        return [
            // userId rules
            ['user_id', 'integer'],
            ['user_id', 'required'],
            // source rules
            ['source', 'required'],
            ['source', 'string', 'max' => 255],
            // source_id rules
            ['source_id', 'required'],
            ['source_id', 'string', 'max' => 255],
        ];
    }
}