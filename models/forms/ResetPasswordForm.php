<?php

namespace sadovojav\user\models\forms;

use sadovojav\user\Module;
use sadovojav\user\models\User;
use yii\base\InvalidParamException;

/**
 * Class ResetPasswordForm
 * @package sadovojav\user\models\forms
 */
class ResetPasswordForm extends \yii\base\Model
{
    public $password;

    private $_user;

    /**
     * Creates a form model given a token.
     * @param  string $token
     * @param  array $config name-value pairs that will be used to initialize the object properties
     * @throws \yii\base\InvalidParamException if token is empty or not valid
     */
    public function __construct($token, $config = [])
    {
        if (empty($token) || !is_string($token)) {
            throw new InvalidParamException('Password reset token cannot be blank.');
        }

        $this->_user = User::findByPasswordResetToken($token);

        if (!$this->_user) {
            throw new InvalidParamException('Wrong password reset token.');
        }

        parent::__construct($config);
    }

    public function rules()
    {
        return [
            // password rules
            ['password', 'required'],
            ['password', 'string', 'min' => 6]
        ];
    }

    public function attributeLabels()
    {
        return [
            'password' => Module::t('user', 'Password')
        ];
    }

    /**
     * Resets password.
     * @return boolean if password was reset.
     */
    public function resetPassword()
    {
        $user = $this->_user;

        $user->setPassword($this->password);
        $user->removePasswordResetToken();

        return $user->save(false);
    }
}
