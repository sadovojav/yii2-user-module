<?php

namespace sadovojav\user\models\forms;

use Yii;
use sadovojav\user\Module;
use sadovojav\user\models\User;

/**
 * Class PasswordResetRequestForm
 * @package sadovojav\user\models\forms
 */
class PasswordResetRequestForm extends \yii\base\Model
{
    public $email;

    private $_user;

    public function rules()
    {
        return [
            // email rules
            ['email', 'required'],
            ['email', 'trim'],
            ['email', 'email'],
            ['email', 'exist',
                'targetClass' => '\sadovojav\user\models\User',
                'filter' => ['status' => User::STATUS_ACTIVE],
                'message' => 'There is no user with such email.'
            ],
        ];
    }

    /**
     * Sends an email with a link, for resetting the password.
     * @return boolean whether the email was send
     */
    public function sendEmail()
    {
        $this->_user = User::findOne([
            'status' => User::STATUS_ACTIVE,
            'email' => $this->email,
        ]);

        if ($this->_user) {
            if (!User::isPasswordResetTokenValid($this->_user->password_reset_token)) {
                $this->_user->generatePasswordResetToken();
            }

            if ($this->_user->save()) {
                $view = [
                    'html' => 'html/password-reset',
                    'text' => 'text/password-reset'
                ];

                $mailer = Yii::$app->mailer;
                $mailer->viewPath = Module::getInstance()->mailViewPath;

                return Yii::$app->mailer->compose($view, ['user' => $this->_user])
                    ->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->name . ' robot'])
                    ->setTo($this->email)
                    ->setSubject('Password reset for ' . Yii::$app->name)
                    ->send();
            }
        }

        return false;
    }
}
