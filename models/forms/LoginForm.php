<?php

namespace sadovojav\user\models\forms;

use Yii;
use sadovojav\user\Module;
use sadovojav\user\models\User;

/**
 * Class LoginForm
 * @package sadovojav\user\models\forms
 */
class LoginForm extends \yii\base\Model
{
    public $username;
    public $password;
    public $rememberMe = true;

    private $user = false;

    public function rules()
    {
        return [
            // username rules
            ['username', 'required'],
            ['username', 'trim'],
            // password rules
            ['password', 'required'],
            ['password', 'validatePassword'],
            // rememberMe rules
            ['rememberMe', 'boolean']
        ];
    }

    public function attributeLabels()
    {
        return [
            'username' => Module::t('user', 'User Name'),
            'password' => Module::t('user', 'Password'),
            'rememberMe' => Module::t('user', 'Remember me'),
        ];
    }

    /**
     * Set username
     * @param mixed $username
     */
    public function setUsername($username)
    {
        $this->username = $username;
    }

    /**
     * Set password
     * @param mixed $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();

            if (!$user || !$user->validatePassword($this->password)) {
                $this->addError($attribute, 'Incorrect username or password.');
            }
        }
    }

    /**
     * Logs in a user using the provided username and password.
     * @return boolean whether the user is logged in successfully
     */
    public function login()
    {
        if ($this->validate()) {
            return Yii::$app->user->login($this->getUser(), $this->rememberMe ? Module::getInstance()->rememberFor : 0);
        } else {
            return false;
        }
    }

    /**
     * Finds user by [[username]]
     * @return User|null
     */
    public function getUser()
    {
        if ($this->user === false) {
            $attribute = strpos($this->username, '@') ? 'email' : 'username';

            $this->user = User::find()->where($attribute . ' = :login AND status = :status', [
                ':login' => $this->username,
                ':status' => User::STATUS_ACTIVE
            ])->one();
        }

        return $this->user;
    }
}
